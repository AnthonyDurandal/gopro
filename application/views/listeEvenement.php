<section id="about" class="light">
    <header class="title">
        <h2>Liste des evenements organises par differents universites.</h2>
    </header>

      <div class="container" >
        <div class="row table-row">
          <div class="col-sm-6 hidden-xs">
            <div class="section-content">
              <div class="big-image" style="background-image:url(images/1.jpg)"></div>
            </div>
          </div>
              <div class="about-content left animated" data-animate="fadeInLeft">
                <div class="about-detail">
                  <h4>Guide d'orientation professionnelle</h4>
                  <p ><span>"Le choix qu'on fait aujourd'hui definira notre demain"</span>.Ainsi pour un avenir plus sure,l'equipe de GoPro vous offre une assistance dans la suite de votre vie proffessionnelle.</p>
                </div>
              </div>
        </div> <!-- /.row table-row -->
      </div> <!-- /.container -->
    </section>