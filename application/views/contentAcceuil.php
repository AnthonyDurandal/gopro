    
    <section id="about" class="light">
      <header class="title">
        <h2>A propos</h2>
        <p>GoPro est guide proffessionnel créer pour aider les <strong>jeunes</strong> dans leur choix d'étude ou de métier.</p>
      </header>

      <div class="container" >
        <div class="row table-row">
          <div class="col-sm-6 hidden-xs">
            <div class="section-content">
              <div class="big-image" style="background-image:url(assets/images/1.jpg)"></div>
            </div>
          </div>
              <div class="about-content left animated" data-animate="fadeInLeft">
                <div class="about-detail">
                  <h4>Guide d'orientation professionnelle</h4>
                  <p ><span>"Le choix qu'on fait aujourd'hui definira notre demain"</span>.Ainsi pour un avenir plus sure,l'equipe de GoPro vous offre une assistance dans la suite de votre vie proffessionnelle.</p>
                </div>
              </div>
              <div class="about-content left animated" data-animate="fadeInLeft">
                <div class="about-detail">
                  <h4>Plateforme de rencontre Universitaire et Etudiant</h4>
                  <p >En collaboration avec les plus grandes universitaire de l'île, GoPro est un attout très performant pour les recherches d'établissement universitaire.</p>
                </div>
              </div>

              <div class="about-content left animated" data-animate="fadeInLeft">
                <div class="about-detail">
                  <h4>Un vaste univers de métier</h4>
                  <p>On propose un vaste choix de métier avec les compétence requis pour,l'intervalle de salaire de chaque domaine et même des offres d'emplois pour ce qui sont intérrésser.</p>
                </div>
              </div>
        </div> <!-- /.row table-row -->
      </div> <!-- /.container -->
    </section>
  
  
      <!-- PORTFOLIO -->

    <section id="portfolio" class="light">
      <header class="title">
        <h2>Filière</h2>
        <p>Voici des propositions de metier que vous pouvez découvrir et même postuler si certains sont à votre goût.</p>
      </header>

      <div class="container-fluid">
        <div class="row">
          <div class="container-metier">
            <!-- PORTFOLIO OBJECT -->
            <script type="text/javascript">
            <?php //echo ($listeFiliereRandom[0]['id']) ?>
            var data = <?php echo json_encode($listeFiliereRandom); ?>

            // alert(data);
            var metier = [{
                category : data[0]['id'],
                      image : "assets/images/"+data[0]['intitule']+".jpg",
                      title : data[0]['intitule']+"</span>",
                      link : "<?php echo base_url('VersFicheFiliere/index/')?>"+"/"+data[0]['id'] ,
                      text : data[0]["descriptionFiliere"]

            }]
            for (let index = 1; index < data.length; index++) {
              metier.push({
                    category : data[index]['id'],
                    image : "assets/images/"+data[index]['intitule']+".jpg",
                    title : data[index]['intitule']+"</span>",
                    link : "<?php echo base_url('VersFicheFiliere/index/')?>"+"/"+data[index]['id'] , 
                    text : data[index]["descriptionFiliere"]
                  })
            }
            
            </script>
          </div>
        </div>
      </div>
    </section>


    <!-- TEAM -->

    <section id="portfolio" class="light" style="margin-top: 50px;">
      <header class="title">
        <h2>Université</h2>
        <p>Voici des propositions de fillière que vous pouvez découvrir .</p>
      </header>

      <div class="container-fluid">

        <div class="row">
          <div class="container-portfolio">
            <!-- PORTFOLIO OBJECT -->
            <script type="text/javascript">
            var data = <?php echo json_encode($listeUniversiteRandom); ?>

            var portfolio = [{
                category : data[0]['id'],
                      title : "assets/images/"+data[0]['nom'],
                      image :"assets/images/"+data[0]['nom']+".jpg",
                      link : "<?php echo base_url('VersFicheUniversite/ficheUniversite/')?>"+"/"+data[0]['id'] ,
                      text : "assets/images/"+data[0]['description']

            }]
            for (let index = 1; index < data.length; index++) {
                portfolio.push({
                      category : data[index]['id'],
                      image :"assets/images/"+data[index]['nom']+".jpg",
                      title : data[index]['nom']+"</span>",
                      link : "<?php echo base_url('VersFicheUniversite/ficheUniversite/')?>"+"/"+data[index]['id'] ,
                      text :"assets/images/"+data[index]['description']
                    })
            }
            </script>
          </div>
        </div>
      </div>
    </section>