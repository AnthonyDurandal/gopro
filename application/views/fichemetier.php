<!-- ABOUT -->

<section id="about" class="light">
  <header class="title">
    <h2>Université Catholique de Madagascar</h2>
  </header>

  <div class="container" >
    <div class="row table-row">
      <div class="col-sm-6 hidden-xs">
        <div class="section-content">
          <div class="big-image" style="background-image:url(images/1.jpg)"></div>
        </div>
      </div>
          <div class="about-content left animated" data-animate="fadeInLeft">
            <div class="about-detail">
              <h4>Guide d'orientation professionnelle</h4>
              <p ><span>"Le choix qu'on fait aujourd'hui definira notre demain"</span>.Ainsi pour un avenir plus sure,l'equipe de GoPro vous offre une assistance dans la suite de votre vie proffessionnelle.</p>
            </div>
          </div>
          <div class="about-content left animated" data-animate="fadeInLeft">
            <div class="about-detail">
              <h4>Plateforme de rencontre Universitaire et Etudiant</h4>
              <p >En collaboration avec les plus grandes universitaire de l'île, GoPro est un attout très performant pour les recherches d'établissement universitaire.</p>
            </div>
          </div>

          <div class="about-content left animated" data-animate="fadeInLeft">
            <div class="about-detail">
              <h4>Un vaste univers de métier</h4>
              <p>On propose un vaste choix de métier avec les compétence requis pour,l'intervalle de salaire de chaque domaine et même des offres d'emplois pour ce qui sont intérrésser.</p>
            </div>
          </div>
    </div> <!-- /.row table-row -->
  </div> <!-- /.container -->
</section>


  <!-- PORTFOLIO -->

<section id="portfolio" class="light">
  <header class="title">
    <h2>Fillière</h2>
    <p>Voici les fillières proposé par l'établissement.</p>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
        var portfolio = [
                {
                  category : "branding",
                  image : "images/p-1.png",
                  title : "Charming <span>Roxy</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-2.png",
                  title : "Fresh <span>Bubba</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-3.png",
                  title : "Wild <span>Romeo</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "video",
                  image : "images/p-4.png",
                  title : "Strange <span>Dexter</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "branding",
                  image : "images/p-5.png",
                  title : "Free <span>Sarah</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "printing",
                  image : "images/p-6.png",
                  title : "Chico <span>Silly</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "printing",
                  image : "images/p-7.png",
                  title : "IG <span>Shop</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "printing",
                  image : "images/p-8.png",
                  title : "Tana <span>Samawa</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                }
            ];
        </script>
      </div>
    </div>
  </div>
</section>
