<script type="text/javascript">
    var dataHeader = [
                        {
                          bigImage :"images/slide-1.jpg",
                          title : "C'est votre choix d'aujourd'hui qui designera votre demain",
            author : "Templatestock"
                        },
                        {
                          bigImage :"images/slide-2.jpg",
                          title : "Demain,ce serra à votre tour de régner.Soyer digne...",
                          author : "Templatestock"
                        },
                        {
                          bigImage :"images/slide-3.jpg",
                          title : "Votre succés ne depend que de vous...",
                          author : "Templatestock"
                        }
                    ],
        loaderSVG = new SVGLoader(document.getElementById('loader'), {speedIn : 0, speedOut : 0, easingIn : mina.easeinout});
        loaderSVG.show()
    </script>
<div class="svg-wrap">
  <svg width="64" height="64" viewBox="0 0 64 64">
    <path id="arrow-left" d="M26.667 10.667q1.104 0 1.885 0.781t0.781 1.885q0 1.125-0.792 1.896l-14.104 14.104h41.563q1.104 0 1.885 0.781t0.781 1.885-0.781 1.885-1.885 0.781h-41.563l14.104 14.104q0.792 0.771 0.792 1.896 0 1.104-0.781 1.885t-1.885 0.781q-1.125 0-1.896-0.771l-18.667-18.667q-0.771-0.813-0.771-1.896t0.771-1.896l18.667-18.667q0.792-0.771 1.896-0.771z"></path>
  </svg>

  <svg width="64" height="64" viewBox="0 0 64 64">
    <path id="arrow-right" d="M37.333 10.667q1.125 0 1.896 0.771l18.667 18.667q0.771 0.771 0.771 1.896t-0.771 1.896l-18.667 18.667q-0.771 0.771-1.896 0.771-1.146 0-1.906-0.76t-0.76-1.906q0-1.125 0.771-1.896l14.125-14.104h-41.563q-1.104 0-1.885-0.781t-0.781-1.885 0.781-1.885 1.885-0.781h41.563l-14.125-14.104q-0.771-0.771-0.771-1.896 0-1.146 0.76-1.906t1.906-0.76z"></path>
  </svg>
</div>


<!-- MAIN CONTENT -->



<!-- ABOUT -->

<section id="about" class="light">
  <header class="title">
    <h2>Liste d'Université partenaire</h2>
  </header>

  <div class="container" >
  <?php foreach($listeUniversite as $universite ) {?>
   <a href="<?php echo base_url('VersFicheUniversite/ficheUniversite/')."/".$universite->getId();?>"><div  class="big-image" style=" width: 400px;height: 350px;background-image:url(<?php echo base_url('assets/images/1.jpg');?> ; background-size: cover;"></div>
        
      <div class="row table-row" style="margin-top:5px">
        <div class="col-sm-6 hidden-xs">
          <div class="section-content">
            <div  style=" width:450px;  height: 300px; background-image:url(<?php echo base_url('assets/images/')."/".$universite->getNom().'.jpg';?>);background-size: cover ;   "></div>
          </div>
        </div>
            <div class="about-content left animated" data-animate="fadeInLeft">
              <div class="about-detail">
               </h2>
                <h2><?php echo $universite->getNom();?></h2>
                <p><?php echo $universite->getDescription();?>.</p>
              </div>
            </div>
      </div>
    </a>
    <?php } ?>
  </div> <!-- /.container -->
</section>