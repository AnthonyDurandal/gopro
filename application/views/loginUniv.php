<!DOCTYPE html>
<html style="font-size: 16px;">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Université">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>LoginUniversite</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/univ/nicepage.css'); ?>" media="screen">
    <link rel="stylesheet" href="<?php echo base_url('assets/univ/LoginUniversite.css'); ?>" media="screen">
    <script class="u-script" type="text/javascript" src="<?php echo base_url('assets/univ/jquery.js'); ?>" defer=""></script>
    <script class="u-script" type="text/javascript" src="<?php echo base_url('assets/univ/nicepage.js'); ?>" defer=""></script>
    <meta name="generator" content="Nicepage 3.23.2, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster:400">
    
    
    <script type="application/ld+json">{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": ""
}</script>
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="LoginUniversite">
    <meta property="og:type" content="website">
  </head>
  <body class="u-body"><header class="u-clearfix u-grey-90 u-header u-header" id="sec-82cc"><div class="u-align-left-lg u-align-left-md u-align-left-sm u-align-left-xs u-clearfix u-sheet u-sheet-1"></div></header>
    <section class="u-clearfix u-grey-90 u-section-1" id="sec-5f8f">
      <div class="u-align-left u-clearfix u-sheet u-sheet-1">
        <img class="u-image u-image-default u-preserve-proportions u-image-1" src="<?php echo base_url('assets/images/logo.png'); ?>" alt="" data-image-width="300" data-image-height="300" data-animation-name="slideIn" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="Down">
        <h1 class="u-custom-font u-font-lobster u-text u-text-default u-text-1" data-animation-name="fadeIn" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">&nbsp;Université</h1>
        <div class="u-form u-form-1">
          <form action="#" method="POST" class="u-clearfix u-form-spacing-41 u-form-vertical u-inner-form" source="custom" name="form" style="padding: 34px;">
            <div class="u-form-group u-form-name">
              <label for="name-6688" class="u-form-control-hidden u-label"></label>
              <input type="text" placeholder="Entrez votre nom" id="name-6688" name="name" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="">
            </div>
            <div class="u-form-password u-form-group">
              <label for="password-6688" class="u-form-control-hidden u-label"></label>
              <input type="password" placeholder="Entrez votre mot de passe" id="password-6688" name="password" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="">
            </div>
            <div class="u-align-center u-form-group u-form-submit">
              <a href="#" class="u-border-2 u-border-white u-btn u-btn-submit u-button-style u-hover-palette-1-base u-none u-text-white u-btn-1">Se connecter</a>
              <input type="submit" value="submit" class="u-form-control-hidden">
            </div>
            <div class="u-form-send-error u-form-send-message"> Impossible de se connecter a votre compte. Veuillez corriger les erreurs puis réessayer. </div>
            <input type="hidden" value="" name="recaptchaResponse">
          </form>
        </div>
      </div>
    </section>
    
    
    <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-8fb6"><div class="u-clearfix u-sheet u-sheet-1">
        <p class="u-small-text u-text u-text-variant u-text-1">Tous droits reservés</p>
      </div></footer>
    <section class="u-backlink u-clearfix u-grey-80">
      <a class="u-link" href="https://nicepage.com/website-templates" target="_blank">
        <span>Website Templates</span>
      </a>
      <p class="u-text">
        <span>created with</span>
      </p>
      <a class="u-link" href="https://nicepage.com/" target="_blank">
        <span>Website Builder Software</span>
      </a>. 
    </section>
  </body>
</html>