<section id="about" class="light">
  <header class="title">
    <h2><?php echo $branche['intitule']; ?></h2>
  </header>

  <div class="container" >
    <div class="row table-row">
      <div class="col-sm-6 hidden-xs">
        <div class="section-content">
          <div class="big-image" style="background-image:url(images/1.jpg)"></div>
        </div>
      </div>
      <div class="about-content left animated" data-animate="fadeInLeft">
            <div class="about-detail">
              <h4>Description</h4>
              <p ><?php 
                $span = explode($branche['descriptionFiliere'],'.');
                for($i=0;$i<count($span);$i++){
              ?>
              <span><?php echo $span[$i];?></span>

              <?php }?></p>
            </div>
          </div>
          
         
    </div> <!-- /.row table-row -->
  </div> <!-- /.container -->
</section>


  <!-- PORTFOLIO -->

<section id="portfolio" class="light">
  <header class="title">
    <h2>Liste Metier</h2>
    <p>Voici les metiers proposé par cette branche.</p>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
          var data = <?php echo json_encode($listeMetier); ?>

          var portfolio = [{
                      category : data[0]['id'],
                      image : "assets/images/"+data[0]['nomMetier']+".jpg",
                      title : data[0]['nomMetier']+"</span>",
                      link : "<?php echo base_url('VersFicheBranche/index/')?>"+data[0]['id'],
                      text : data[0]["descriptionBranche"]

            }]
            for (let index = 1; index < data.length; index++) {
              portfolio.push({
                    category : data[index]['id'],
                    image : "assets/images/"+data[index]['nomMetier']+".jpg",
                    title : data[index]['nomMetier']+"</span>",
                    link : "<?php echo base_url('VersFicheMetier/index/')?>"+data[index]['id'],
                    text : data[index]["descriptionBranche"]
                  })
          }
        </script>
      </div>
    </div>
  </div>
</section>