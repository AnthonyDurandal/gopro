<script type="text/javascript">
    var dataHeader = [
                        {
                          bigImage :"images/slide-1.jpg",
                          title : "C'est votre choix d'aujourd'hui qui designera votre demain",
            author : "Templatestock"
                        },
                        {
                          bigImage :"images/slide-2.jpg",
                          title : "Demain,ce serra à votre tour de régner.Soyer digne...",
                          author : "Templatestock"
                        },
                        {
                          bigImage :"images/slide-3.jpg",
                          title : "Votre succés ne depend que de vous...",
                          author : "Templatestock"
                        }
                    ],
        loaderSVG = new SVGLoader(document.getElementById('loader'), {speedIn : 0, speedOut : 0, easingIn : mina.easeinout});
        loaderSVG.show()
    </script>
<div class="svg-wrap">
  <svg width="64" height="64" viewBox="0 0 64 64">
    <path id="arrow-left" d="M26.667 10.667q1.104 0 1.885 0.781t0.781 1.885q0 1.125-0.792 1.896l-14.104 14.104h41.563q1.104 0 1.885 0.781t0.781 1.885-0.781 1.885-1.885 0.781h-41.563l14.104 14.104q0.792 0.771 0.792 1.896 0 1.104-0.781 1.885t-1.885 0.781q-1.125 0-1.896-0.771l-18.667-18.667q-0.771-0.813-0.771-1.896t0.771-1.896l18.667-18.667q0.792-0.771 1.896-0.771z"></path>
  </svg>

  <svg width="64" height="64" viewBox="0 0 64 64">
    <path id="arrow-right" d="M37.333 10.667q1.125 0 1.896 0.771l18.667 18.667q0.771 0.771 0.771 1.896t-0.771 1.896l-18.667 18.667q-0.771 0.771-1.896 0.771-1.146 0-1.906-0.76t-0.76-1.906q0-1.125 0.771-1.896l14.125-14.104h-41.563q-1.104 0-1.885-0.781t-0.781-1.885 0.781-1.885 1.885-0.781h41.563l-14.125-14.104q-0.771-0.771-0.771-1.896 0-1.146 0.76-1.906t1.906-0.76z"></path>
  </svg>
</div>


<!-- MAIN CONTENT -->



  <!-- PORTFOLIO -->

<section id="metier" class="light" style="margin-top:50px">
  <header class="title">
    <h2>Metier</h2>
    <p>Voici des propositions de metier que vous pouvez découvrir et même postuler si certains sont à votre goût.</p>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-metier">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
        var metier = [
                {
                  category : "branding",
                  image : "images/juge2.jpg",
                  title : "Juriste<span></span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/pilote-avion.jpg",
                  title : "Pilote<span></span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/compta.jpg",
                  title : "Comptable <span></span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "video",
                  image : "images/ingenieuragronome.jpg",
                  title : "<span>Agronome</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "branding",
                  image : "images/photographe.jpg",
                  title : "Photographe",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "printing",
                  image : "images/devel.jpeg",
                  title : "Developpeur",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "printing",
                  image : "images/economiste.jpeg",
                  title : "Economiste",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "printing",
                  image : "images/p-8.png",
                  title : "Tana <span>Samawa</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                }
            ];
        </script>
      </div>
    </div>
  </div>
</section>  

<section id="portfolio" class="light" style="margin-top:50px">
  <header class="title">
    <h2>Scientifique</h2>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
        var portfolio = [
                {
                  category : "branding",
                  image : "images/p-1.png",
                  title : "Charming <span>Roxy</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-2.png",
                  title : "Fresh <span>Bubba</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-3.png",
                  title : "Wild <span>Romeo</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "video",
                  image : "images/p-4.png",
                  title : "Strange <span>Dexter</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                }
            ];
        </script>
      </div>
    </div>
  </div>
</section>

<section id="portfolio" class="light" style="margin-top:50px">
  <header class="title">
    <h2>Littéraire</h2>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
        var portfolio = [
                {
                  category : "branding",
                  image : "images/p-1.png",
                  title : "Charming <span>Roxy</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-2.png",
                  title : "Fresh <span>Bubba</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-3.png",
                  title : "Wild <span>Romeo</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "video",
                  image : "images/p-4.png",
                  title : "Strange <span>Dexter</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                }
            ];
        </script>
      </div>
    </div>
  </div>
</section>

<section id="portfolio" class="light" style="margin-top:50px">
  <header class="title">
    <h2>Divers</h2>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
        var portfolio = [
                {
                  category : "branding",
                  image : "images/p-1.png",
                  title : "Charming <span>Roxy</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-2.png",
                  title : "Fresh <span>Bubba</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-3.png",
                  title : "Wild <span>Romeo</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "video",
                  image : "images/p-4.png",
                  title : "Strange <span>Dexter</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                }
            ];
        </script>
      </div>
    </div>
  </div>
</section>