<section id="about" class="light">
  <header class="title">
    <h2>FILIERE</h2>
  </header>

  <div class="container" >
    <div class="row table-row">
      <div class="col-sm-6 hidden-xs">
        <div class="section-content">
          <div  style="width: 400px;height: 350px;
background-size: cover;background-image:url(<?php echo base_url('assets/images/')."/".$filiere['intitule'].'.jpg';?>)"></div>
        </div>
      </div>
      <div class="about-content left animated" data-animate="fadeInLeft">
            <div class="about-detail">
              <h4>Description</h4>
              <p ><?php 
                $span = explode($filiere['descriptionFiliere'],'.');
                for($i=0;$i<count($span);$i++){
              ?>
              <span><?php echo $span[$i];?></span>

              <?php }?></p>
            </div>
          </div>
          
         
    </div> <!-- /.row table-row -->
  </div> <!-- /.container -->
</section>


  <!-- PORTFOLIO -->

<section id="portfolio" class="light">
  <header class="title">
    <h2>Liste Branche</h2>
    <p>Voici les fillières proposé par l'établissement.</p>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
          var data = <?php echo json_encode($listeBranche); ?>

          var portfolio = [{
                      category : data[0]['id'],
                      image : "assets/images/"+data[0]['intitule']+".jpg",
                      title : data[0]['intitule']+"</span>",
                      link : "",
                      text : data[0]["descriptionBranche"]

            }]
            for (let index = 1; index < data.length; index++) {
              portfolio.push({
                    category : data[index]['id'],
                    image : "assets/images/"+data[index]['intitule']+".jpg",
                    title : data[index]['intitule']+"</span>",
                    link : "",
                    text : data[index]["descriptionBranche"]
                  })
          }
        </script>
      </div>
    </div>
  </div>
</section>