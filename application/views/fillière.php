<script type="text/javascript">
    var dataHeader = [
                        {
                          bigImage :"images/slide-1.jpg",
                          title : "C'est votre choix d'aujourd'hui qui designera votre demain",
                          author : "Templatestock"
                        },
                        {
                          bigImage :"images/slide-2.jpg",
                          title : "Demain,ce serra à votre tour de régner.Soyer digne...",
                          author : "Templatestock"
                        },
                        {
                          bigImage :"images/slide-3.jpg",
                          title : "Votre succés ne depend que de vous...",
                          author : "Templatestock"
                        }
                    ],
        loaderSVG = new SVGLoader(document.getElementById('loader'), {speedIn : 0, speedOut : 0, easingIn : mina.easeinout});
        loaderSVG.show()
    </script>

<!-- MAIN CONTENT -->


  <!-- PORTFOLIO -->

<section id="portfolio" class="light" style="margin-top: 50px;">
  <header class="title">
    <h2>Fillière</h2>
    <p>Voici des propositions de fillière que vous pouvez découvrir .</p>
  </header>

  <div class="container-fluid">

    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
        var portfolio = [
                {
                  category : "branding",
                  image : "images/p-1.png",
                  title : "AGRO<span>NOMIE</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-2.png",
                  title : "<span>Informatique</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-3.png",
                  title : "Wild <span>Romeo</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "video",
                  image : "images/p-4.png",
                  title : "<span>Droit</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "branding",
                  image : "images/p-5.png",
                  title : "Eco<span>nomie</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "printing",
                  image : "images/p-6.png",
                  title : "Multi <span>Media</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "printing",
                  image : "images/compte.jpg",
                  title : "Compt<span>abilité</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "printing",
                  image : "images/aviati.jpg",
                  title : "Aviation",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                }
            ];
        </script>
      </div>
    </div>
  </div>
</section>

<section id="portfolio" class="light" style="margin-top:50px">
  <header class="title">
    <h2>Scientifique</h2>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
        var portfolio = [
                {
                  category : "branding",
                  image : "images/p-1.png",
                  title : "Charming <span>Roxy</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-2.png",
                  title : "Fresh <span>Bubba</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-3.png",
                  title : "Wild <span>Romeo</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "video",
                  image : "images/p-4.png",
                  title : "Strange <span>Dexter</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                }
            ];
        </script>
      </div>
    </div>
  </div>
</section>

<section id="portfolio" class="light" style="margin-top:50px">
  <header class="title">
    <h2>Littéraire</h2>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
        var portfolio = [
                {
                  category : "branding",
                  image : "images/p-1.png",
                  title : "Charming <span>Roxy</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-2.png",
                  title : "Fresh <span>Bubba</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-3.png",
                  title : "Wild <span>Romeo</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "video",
                  image : "images/p-4.png",
                  title : "Strange <span>Dexter</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                }
            ];
        </script>
      </div>
    </div>
  </div>
</section>

<section id="portfolio" class="light" style="margin-top:50px">
  <header class="title">
    <h2>Divers</h2>
  </header>

  <div class="container-fluid">
    <div class="row">
      <div class="container-portfolio">
        <!-- PORTFOLIO OBJECT -->
        <script type="text/javascript">
        var portfolio = [
                {
                  category : "branding",
                  image : "images/p-1.png",
                  title : "Charming <span>Roxy</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-2.png",
                  title : "Fresh <span>Bubba</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "graphic",
                  image : "images/p-3.png",
                  title : "Wild <span>Romeo</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                },
                {
                  category : "video",
                  image : "images/p-4.png",
                  title : "Strange <span>Dexter</span>",
                  link : "#none",
                  text : "Lorem ipsum Dolor in minim fugiat ut nisi occaecat fugiat. Lorem ipsum Quis tempor Ut enim officia deserunt consectetur."
                }
            ];
        </script>
      </div>
    </div>
  </div>
</section>