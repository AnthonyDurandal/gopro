<!DOCTYPE html>
<html style="font-size: 16px;">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Nom Universite, Créer un évènement">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>AccueilUniversite</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/univ/nicepage.css'); ?>" media="screen">
    <link rel="stylesheet" href="<?php echo base_url('assets/univ/AccueilUniversite.css'); ?>" media="screen">
    <script class="u-script" type="text/javascript" src="<?php echo base_url('assets/univ/jquery.js'); ?>" defer=""></script>
    <!-- <script class="u-script" type="text/javascript" src="<?php #echo base_url('assets/univ/nicepage.js'); ?>" defer=""></script> -->
    <meta name="generator" content="Nicepage 3.23.2, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster:400|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
    
    
    
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": ""
        }
    </script>
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="AccueilUniversite">
    <meta property="og:type" content="website">
  </head>
  <body class="u-body"><header class="u-clearfix u-grey-90 u-header u-header" id="sec-82cc"><div class="u-align-left-lg u-align-left-md u-align-left-sm u-align-left-xs u-clearfix u-sheet u-sheet-1"></div></header> 
    <section class="u-clearfix u-grey-90 u-section-1" id="sec-6fb7">
      <div class="u-align-left u-clearfix u-sheet u-sheet-1">
        <h1 class="u-custom-font u-font-lobster u-text u-text-default u-text-1" data-animation-name="fadeIn" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="">Nom Universite</h1>
        <img class="u-image u-image-default u-preserve-proportions u-image-1" src="<?php echo base_url('assets/images/logo.png'); ?>" alt="" data-image-width="300" data-image-height="300" data-animation-name="slideIn" data-animation-duration="1000" data-animation-delay="0" data-animation-direction="Down">
        <h4 class="u-custom-font u-font-montserrat u-text u-text-default u-text-2">Historique des évènements</h4>
        <div class="u-expanded-width u-table u-table-responsive u-table-1">
          <table class="u-table-entity">
            <colgroup>
              <col width="20%">
              <col width="20%">
              <col width="20%">
              <col width="20%">
              <col width="20%">
            </colgroup>
            <tbody class="u-table-body">
                <tr style="height: 64px;">
                    <td class="u-border-1 u-border-grey-30 u-table-cell">Type d'évènement</td>
                    <td class="u-border-1 u-border-grey-30 u-table-cell">Description</td>
                    <td class="u-border-1 u-border-grey-30 u-table-cell">Date de debut</td>
                    <td class="u-border-1 u-border-grey-30 u-table-cell">Date de fin</td>
                    <td class="u-border-1 u-border-grey-30 u-table-cell"></td>
                </tr>
                <?php foreach($listeEvenement as $row) {?>
                    <tr style="height: 65px;">
                        <td class="u-border-1 u-border-grey-30 u-table-cell"><?php echo $row->getIdTypeEvenement()?></td>
                        <td class="u-border-1 u-border-grey-30 u-table-cell"><?php echo $row->getDescription()?></td>
                        <td class="u-border-1 u-border-grey-30 u-table-cell"><?php echo $row->getDateDebut()?></td>
                        <td class="u-border-1 u-border-grey-30 u-table-cell"><?php echo $row->getDateFin()?></td>
                        <td class="u-border-1 u-border-grey-30 u-table-cell">
                        <a class="u-border-none u-btn u-button-style u-grey-5 u-hover-palette-1-dark-1 u-btn-1" href="deleteEvenement?id=<?php echo $row->getId()?>">Supprimer</a>
                        </td>
                    </tr>
                <?php } ?>
          </table>
        </div>
      </div>
    </section>
    <section class="u-clearfix u-grey-90 u-section-2" id="sec-b69a">
      <div class="u-align-left u-clearfix u-sheet u-sheet-1">
        <h1 class="u-custom-font u-font-lobster u-text u-text-default u-text-1">Créer un évènement</h1>
        <div class="u-form u-form-1">
          <form action="ajoutEvenement" method="POST" class="u-clearfix u-form-spacing-41 u-form-vertical u-inner-form" source="custom" name="form" style="padding: 34px;">
            <div class="u-form-group u-form-select u-form-group-1">
              <label for="select-5683" class="u-form-control-hidden u-label"></label>
              <div class="u-form-select-wrapper">
                <select id="select-5683" name="select" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white">
                    <?php foreach($listeTypeEvenement as $row) {?>
                        <option value="<?php echo $row->getId()?>"><?php echo $row->getIntitule()?></option>
                    <?php } ?>
                </select>
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="12" version="1" class="u-caret"><path fill="currentColor" d="M4 8L0 4h8z"></path></svg>
              </div>
            </div>
            <div class="u-form-date u-form-group u-form-group-2">
              <label for="date-05f3" class="u-form-control-hidden u-label"></label>
              <input type="date" placeholder="MM/DD/YYYY" id="date-05f3" name="date" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="">
            </div>
            <div class="u-form-date u-form-group u-form-group-3">
              <label for="date-3e54" class="u-form-control-hidden u-label"></label>
              <input type="date" placeholder="MM/DD/YYYY" id="date-3e54" name="date1" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required="">
            </div>
            <div class="u-form-group u-form-textarea u-form-group-4">
              <label for="textarea-72ff" class="u-form-control-hidden u-label"></label>
              <textarea rows="4" cols="50" id="textarea-72ff" name="textarea" class="u-border-1 u-border-grey-30 u-input u-input-rectangle u-white" required=""></textarea>
            </div>
            <div class="u-align-left u-form-group u-form-submit">
              <!-- <a href="#" class="u-border-2 u-border-white u-btn u-btn-submit u-button-style u-hover-palette-1-base u-none u-text-white u-btn-1"></a> -->
              <input type="submit" value="Ajouter evenement" class="u-border-2 u-border-white u-btn u-btn-submit u-button-style u-hover-palette-1-base u-none u-text-white u-btn-1">
            </div>
          </form>
        </div>
        <p class="u-align-left u-custom-item u-text u-text-2"> Type d'évènement</p>
        <p class="u-align-left u-custom-item u-text u-text-3"> Date de debut</p>
        <p class="u-align-left u-custom-item u-text u-text-4"> Date de fin</p>
        <p class="u-align-left u-custom-item u-text u-text-5"> Description<br>
        </p>
        <a href="https://nicepage.com/fr/themes-wordpress" class="u-border-1 u-border-active-palette-2-base u-border-hover-palette-1-base u-btn u-button-style u-none u-text-palette-1-base u-btn-2">Se deconnecter</a>
      </div>
    </section>
    
    
    <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-8fb6"><div class="u-clearfix u-sheet u-sheet-1">
        <p class="u-small-text u-text u-text-variant u-text-1">Tous droits reservés</p>
      </div></footer>
    <section class="u-backlink u-clearfix u-grey-80">
      <a class="u-link" href="https://nicepage.com/website-templates" target="_blank">
        <span>Website Templates</span>
      </a>
      <p class="u-text">
        <span>created with</span>
      </p>
      <a class="u-link" href="https://nicepage.com/" target="_blank">
        <span>Website Builder Software</span>
      </a>. 
    </section>
  </body>
</html>