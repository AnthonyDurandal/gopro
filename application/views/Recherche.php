<script type="text/javascript">
    var dataHeader = [
                        {
                          bigImage :"images/slide-1.jpg",
                          title : "C'est votre choix d'aujourd'hui qui designera votre demain",
            author : "Templatestock"
                        },
                        {
                          bigImage :"images/slide-2.jpg",
                          title : "Demain,ce serra à votre tour de régner.Soyer digne...",
                          author : "Templatestock"
                        },
                        {
                          bigImage :"images/slide-3.jpg",
                          title : "Votre succés ne depend que de vous...",
                          author : "Templatestock"
                        }
                    ],
        loaderSVG = new SVGLoader(document.getElementById('loader'), {speedIn : 0, speedOut : 0, easingIn : mina.easeinout});
        loaderSVG.show()
    </script>
<div class="svg-wrap">
  <svg width="64" height="64" viewBox="0 0 64 64">
    <path id="arrow-left" d="M26.667 10.667q1.104 0 1.885 0.781t0.781 1.885q0 1.125-0.792 1.896l-14.104 14.104h41.563q1.104 0 1.885 0.781t0.781 1.885-0.781 1.885-1.885 0.781h-41.563l14.104 14.104q0.792 0.771 0.792 1.896 0 1.104-0.781 1.885t-1.885 0.781q-1.125 0-1.896-0.771l-18.667-18.667q-0.771-0.813-0.771-1.896t0.771-1.896l18.667-18.667q0.792-0.771 1.896-0.771z"></path>
  </svg>

  <svg width="64" height="64" viewBox="0 0 64 64">
    <path id="arrow-right" d="M37.333 10.667q1.125 0 1.896 0.771l18.667 18.667q0.771 0.771 0.771 1.896t-0.771 1.896l-18.667 18.667q-0.771 0.771-1.896 0.771-1.146 0-1.906-0.76t-0.76-1.906q0-1.125 0.771-1.896l14.125-14.104h-41.563q-1.104 0-1.885-0.781t-0.781-1.885 0.781-1.885 1.885-0.781h41.563l-14.125-14.104q-0.771-0.771-0.771-1.896 0-1.146 0.76-1.906t1.906-0.76z"></path>
  </svg>
</div>


<!-- MAIN CONTENT -->



<!-- ABOUT -->

<section id="about" class="light">
  <header class="title">
  	<h2>Rechercher des filières à partir d'un metier:</h2>
    <form action="<?php echo base_url('Fonctionnalite/rechercherFiliere');?>" method="post">
		<select name="idMetier" class="form-control" id="exampleFormControlSelect1">
			<?php foreach($listeMetier as $metier){?>
				<option value="<?php echo $metier->getId() ;?>"><?php echo $metier->getNomMetier() ;?></option>
			<?php }?>
		</select>
		<button type="submit" class="btn btn-primary">Rechercher</button>
	</form>

    <h2>Liste de filière correspondant</h2>
  </header>

  <div class="container" >
   <?php 
if(isset($resultatRecherche)){
	if(count($resultatRecherche)>0){
   foreach($resultatRecherche as $filiere ) {?>

    
      <div class="row table-row" style="margin-top:5px">
        <div class="col-sm-6 hidden-xs">
          <div class="section-content">
            <a href="<?php echo base_url('VersFicheFiliere/index/')."/".$filiere['id'];?>"><div  style=" width: 400px;height: 350px;background-image:url(<?php echo base_url('assets/images/')."/".$filiere['intitule'].'.jpg';?>) ; background-size: cover;"></div>
          </div>
        </div>
            <div class="about-content left animated" data-animate="fadeInLeft">
              <div class="about-detail">
               
                <h4><?php echo $filiere['intitule'];?></h4>
                <p><?php echo $filiere['descriptionFiliere'];?></p>
              </div>
            </div>
          </a>
      </div>
    </a>
    <?php } }
		 ?>
   <?php }
   	else{?>
   		<h2>Aucun resultat.</h2>
   	<?php }?>
   
   


  </div> <!-- /.container -->
</section>