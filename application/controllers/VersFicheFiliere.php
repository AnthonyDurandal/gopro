<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VersFicheFiliere extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($id)
	{
		$this->ajouterCompteurStat($id);
		$this->load->model('Filiere');
		$this->load->model('Branche');

		
		$data['filiere']=$this->Filiere->getFiliereById($id);
		$data['listeBranche']=$this->Branche->getBrancheByIdFiliere($id);
		$data['content']="ficheFiliere.php";

		$this->load->view('Acceuil',$data);
	}
	public function ajouterCompteurStat($id)
	{
		$this->db->query(sprintf("insert into visiteParPage values('ficheFiliere',%s,null,null,sysdate())",$id));
	}
}
