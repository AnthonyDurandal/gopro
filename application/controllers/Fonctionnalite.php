<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fonctionnalite extends CI_Controller{

	public function versSearch()
	{
        $this->load->model('Metier');
        $this->Metier->addConnection($this->db);
        $data['listeMetier'] = $this->Metier->find(null);
        //echo "TONGA";
        $data['content'] = "Recherche.php";
        $this->load->view('Acceuil',$data);
	
    }
    public function rechercherFiliere()
    {
        $this->load->model('Filiere');
        $this->Filiere->addConnection($this->db);
        $this->load->model('Metier');
        $this->Metier->addConnection($this->db);
        $data['listeMetier'] = $this->Metier->find(null);
        $data['resultatRecherche'] = $this->Filiere->getFiliereByMetier($this->input->post('idMetier'));
        //echo "TONGA";
        $data['content'] = "Recherche.php";
        $this->load->view('Acceuil',$data);
    }
    public function versStat(){
    $this->load->model('Filiere');
        $this->Filiere->addConnection($this->db);
        $this->load->model('Universite');
        $this->Universite->addConnection($this->db);
        $data['visiteFiliere'] = $this->Filiere->getVisiteFiliere();
        $data['visiteUniversite'] = $this->Universite->getVisiteUniversite();
        

        $data['content'] = "Statistique.php";
        $this->load->view('Acceuil',$data);
    }
  
    
   

    
}
