<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Redirect extends CI_Controller{

	public function index()
	{

		$this->load->view('loginUniv');
    }
    
    public function versListeUniversite() {
        $this->load->model('DBTable');
        $this->load->model('Universite');
		$this->Universite->addConnection($this->db);
        $data = array();
        $data['listeUniversite'] = $this->Universite->find(null);
        $data['content'] = "ListeUniversite.php";
        //var_dump($data['listeUniversite']);
        $this->load->view('Acceuil',$data);

    }
    public function versListeFiliere() {
        $this->load->model('DBTable');
        $this->load->model('Filiere');
        $this->Filiere->addConnection($this->db);
        $data = array();
        $data['listeFiliere'] = $this->Filiere->find(null);
        $data['content'] = "ListeFiliere.php";
        
        $this->load->view('Acceuil',$data);

    }

    
}
