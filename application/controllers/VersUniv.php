<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VersUniv extends CI_Controller {

	public function index()
	{

		$this->load->view('loginUniv');
    }
    
    public function accueilUniv() {
        $this->load->model('Evenement');
        $this->load->model('DBTable');
        $this->load->model('TypeEvenement');
		$this->Evenement->addConnection($this->db);
        $this->DBTable->addConnection($this->db);
        $this->TypeEvenement->addConnection($this->db);
        $data['listeEvenement']=$this->Evenement->find("");
        $data['listeTypeEvenement']=$this->TypeEvenement->find("");
        $this->load->view('accueiluniv',$data);
    }

    public function ajoutEvenement() {
        if(isset($_POST['select'])&&isset($_POST['date'])&&isset($_POST['date1'])&&isset($_POST['textarea']) ) {
            $this->load->model('Evenement');
            $this->load->model('DBTable');
            $this->load->model('TypeEvenement');
            $this->DBTable->addConnection($this->db);
            $this->Evenement->addConnection($this->db);
            $this->TypeEvenement->addConnection($this->db);
            $data['listeEvenement']=$this->Evenement->find("");
            $data['listeTypeEvenement']=$this->TypeEvenement->find("");
            $this->Evenement->setIdTypeEvenement($_POST['select']);
            $this->Evenement->setIdUniversite(1);
            $this->Evenement->setDateDebut($_POST['date']);
            $this->Evenement->setDateFin($_POST['date1']);
            $this->Evenement->setDescription($_POST['textarea']);
            $this->Evenement->insert();
            $this->load->view('accueiluniv',$data);
        } else {
            $this->load->model('Evenement');
            $this->load->model('DBTable');
            $this->load->model('TypeEvenement');
            $this->DBTable->addConnection($this->db);
            $this->Evenement->addConnection($this->db);
            $this->TypeEvenement->addConnection($this->db);
            $data['listeEvenement']=$this->Evenement->find("");
            $data['listeTypeEvenement']=$this->TypeEvenement->find("");
            $this->load->view('accueiluniv',$data);
        }
      
    }

    public function deleteEvenement() {
        $this->load->model('Evenement');
            $this->load->model('DBTable');
            $this->load->model('TypeEvenement');
            $this->DBTable->addConnection($this->db);
            $this->Evenement->addConnection($this->db);
            $this->TypeEvenement->addConnection($this->db);
            $data['listeEvenement']=$this->Evenement->find("");
            $data['listeTypeEvenement']=$this->TypeEvenement->find("");
            if(isset($_GET['id'])) {
                $this->Evenement->setId($_GET['id']);
                $this->Evenement->Delete();
            }
            
        $this->load->view('accueiluniv',$data);
    }
}
