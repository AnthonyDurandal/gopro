<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scontrol extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$this->load->model('Universite');
		$data['listeUniversite'] = $this->Universite->randomList(3);
		$data['content']="listeUniversite.php";

		$this->load->view('Acceuil',$data);
	}
	public function ficheUniversite($id)
	{
		$this->load->model('Universite');
		$this->load->model('Emplacement');
        $this->load->model('Filiere');
		
        $filieres = Array();
		$data['universite']=$this->Universite->getUniversiteById($id);
        //$refEmplacementUniversite =$this->Emplacement->getEmplacementByIdUniversite($id);
        // var_dump($refEmplacementUniversite);
        //$data['emplacement']=$this->Emplacement->getEmplacementById($refEmplacementUniversite['idEmplacement']);
        //$refEmplacementUniversite =$this->Emplacement->getEmplacementByIdUniversite($id);
       
        $data['filiere'] =  $this->Filiere->getFiliereUniverses();
		$data['content']="Sview.php";



		/*placement des emplacements des universités sur la carte*/
		
		$listeEmplacements = $this->rechercheEmplacement($id);
		$json = json_encode($listeEmplacements, JSON_PRETTY_PRINT);
		
		$data['dataListeEmplacementJson'] =  $json;

		$this->load->view('Acceuil',$data);
	}

	public function ajouterCompteurStat($id)
	{
		$this->db->query(sprintf("insert into visiteParPage values('ficheUniversite',null,null,%s,sysdate())",$id));
	}

	public function rechercheEmplacement($idUniversite)
	{
		$latlng = array();
		$requeteRechercheEmplacements = $this->db->query(sprintf("select * from emplacement e where e.id in (select rel.idEmplacement from relEmplacementUniversite rel where idUniversite=%s)",$idUniversite));
		foreach ($requeteRechercheEmplacements->result_array() as $row) {
			$latlng[] = $row;
		}
		return $requeteRechercheEmplacements->result();
	}
}
