<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Filiere extends DBTable
{
	private $id;
	private $intitule;
    private $descriptionFiliere;

	/**
	 * Class Constructor
	 * @param    $id   
	 * @param    $nom   
	 * @param    $email   
	 * @param    $mdp   
	 */
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or 
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param mixed $nom
     *
     * @return self
     */
    public function setIntitule($i)
    {
        $this->intitule = $i;

        return $this;
    }

    public function getDescriptionFiliere(){
        return $this->descriptionFiliere;
    }
    public function setDescriptionFiliere($d){
        $this->descriptionFiliere = $d;
    }
    public function randomList($nombre)
    {
        $requete = "select * from Filiere order by rand() limit ".$nombre;
        $result = $this->db->query($requete);
        return $result->result_array();
    }
    public function getFiliereById($id)
    {
        $requete = "select * from Filiere where id=".$id;
        $result = $this->db->query($requete);
        return $result->row_array();
    }
    public function getFiliereUniverses()
    {
        $requete0 = "SELECT * FROM Filiere WHERE id in ";
        $requete1 = "(SELECT Distinct(filiere.id) FROM relUniversiteBranche JOIN Branche ON relUniversiteBranche.idBranche = branche.id ";
        $requete2 = "JOIN Filiere ON Branche.idFiliere = filiere.id JOIN universite ON Universite.id = relUniversiteBranche.idUniversite)";
        $requete = $requete0 . $requete1 . $requete2;

        //echo "requete getFiliereUniverses: ".$requete."<br/>" ;
        $result = $this->db->query($requete);
        return $result->result_array();
    }
    public function getFiliereByMetier($idMetier){
         $requete = sprintf("select * from filiere where id IN (select id from Branche where id IN (select idBranche from relBrancheMetier where idMetier=%d)) ",$idMetier);
        $result = $this->db->query($requete);
        return $result->result_array();
    }
    public function getVisiteFiliere(){
        $requete ="select * from  visitesparfiliere";
        $result = $this->db->query($requete);
        return $result->result_array();
    }
   
}

?>