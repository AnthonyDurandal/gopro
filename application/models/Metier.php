<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metier extends DBTable{
	
	private $id;
	private $nomMetier;
	private $descriptionMetier;
	

	function __constructor__($id,$nomMetier,$descriptionMetier,$idBranche){
		$this->setId($id);
		$this->setnomMetier($nomMetier);
		$this->setdescriptionMetier($descriptionMetier);
		$this->setIdBranche($idBranche);
	}
	
	function getId(){
		return $this->id;	
	}
	function getnomMetier(){
		return $this->nomMetier;
	}
	
	function getdescriptionMetier(){
		return $this->descriptionMetier;
	}
	
	function setId($id){
		$this->id = $id;	
	}
	function setnomMetier($nomMetier){
		$this->nomMetier = $nomMetier;
	}
	function setdescriptionMetier($desc){
		$this->descriptionMetier = $desc;
	}
	
	function getMetierByidBranche($idBranche){
		$requete = "select Metier.* from Metier join relBrancheMetier on Metier.id = relBrancheMetier.idMetier where relBrancheMetier.idBranche=".$id;
        $result = $this->db->query($requete);
        return $result->result_array();
	}
}
