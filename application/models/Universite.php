<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Universite extends DBTable
{
	private $id;
	private $nom;
	private $pathLogo;
    private $slogan;
    private $description;
	/**
	 * Class Constructor
	 * @param    $id   
	 * @param    $nom   
	 * @param    $email   
	 * @param    $mdp   
	 */
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getNom()
    {
        return $this->nom;
    }

    
    public function setNom($i)
    {
        $this->nom = $i;

        return $this;
    }

    public function getpathLogo()
    {
        return $this->pathLogo;
    }

    
    public function setpathLogo($i)
    {
        $this->pathLogo = $i;

        return $this;
    }

    public function getSlogan()
    {
        return $this->slogan;
    }

    
    public function setSlogan($i)
    {
        $this->slogan = $i;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    
    public function setDescription($i)
    {
        $this->description = $i;

        return $this;
    }

    public function randomList($nombre)
    {
        $requete = "select * from Universite order by rand() limit 4";
        $result = $this->db->query($requete);
        return $result->result_array();
    }
    
    public function getUniversiteById($id)
    {
        $requete = "select * from Universite where id=".$id;
        $result = $this->db->query($requete);
        return $result->row_array();
    }
    
    public function relUniversiteBranche($id)
    {
        $requete = "select * from relUniversiteBranche where id=".$id;
        $result = $this->db->query($requete);
        return $result->result_array();
    }
    // public function getListBranche()
   public function rechercheUniversiteByFiliere($idFiliere){
        $requete = sprintf("select * from universite where id IN (select idUniversite from relUniversiteBranche where idBranche IN (select idBranche from Branche where idFiliere = 1)) ",$id);
        $result = $this->db->query($requete);
        return $result->result_array();
   }
    public function getVisiteUniversite(){
        $requete ="select * from  visitesparuniversite";
        $result = $this->db->query($requete);
        return $result->result_array();
    }
}

?>