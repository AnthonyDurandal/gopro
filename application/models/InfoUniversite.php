<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Universite extends DBTable{
	
	private $id;
	private $nom;
	private $pathLogo;
	private $slogan;
	private $description;
	private $anneeCreation;
	private $idRefUniversiteBranche;

	function __constructor__($id,$nom,$log,$slogan,$description,$anneeCreation,$idRefUniversiteBranche){
		$this->setId($id);
		$this->setNom($nom);
		$this->setpathLogo($pathLogo);		
		$this->setSlogan($slogan);
		$this->setDescription($description);
		$this->setAnneeCreation($anneeCreation);
		$this->setIdRefUniversiteBranche($idRefUniversiteBranche);

	}
	function getUniversiteById($id){
		$universite = null;
		$requete = "select Universite.* , refUniversiteBranche.id from Universite join refUniversiteBranche on Universite.id = refUniversiteBranche.idUniversite where id=".$id;
		$result = $this->db->query($requete)->row_array();
		foreach($result as $r){
			$universite = new Universite($r["id"],$r['nom'],$r['slogan'],$r['description'],$r['anneeCreation'],$r['idRefUniversiteBranche']);
		}
		return $universite;
	}
	function getUniversiteByIdFiliere($idFiliere){
		$idUniversite = null;
		$requete = "select refUniversiteBranche.idUniversite from branche on filiere.id = branche.idFiliere join refUniversiteBranche on refUniversiteBranche.idBranche = branche.id";
		$idUniversite = $this->db->query($requete)->row_array();
		return $this->getUniversiteById($idUniversite);
	}
	function getId(){
		return $this->id;	
	}
	function getNom(){
		return $this->nom;
	}
	function getpathLogo(){
		return $this->pathLogo;
	}
	function getSlogan(){
		return $this->slogan;
	}
	function getDescription(){
		return $this->description;
	}
	function getAnneeCreation(){
		return $this->anneeCreation;
	}
	function getIdRefUniversiteBranche(){
		return $this->idRefUniversiteBranche;
	}

	function setId($id){
		$this->id = $id;	
	}
	function setNom($nom){
		$this->nom = $nom;
	}
	function setpathLogo($pathLogo){
		$this->pathLogo = $pathLogo;
	}
	function setSlogan($slogan){
		$this->slogan = $slogan;
	}
	function setDescription($desc){
		$this->description = $desc;
	}
	function setAnneeCreation($anneeCreation){
		$this->anneeCreation = $anneeCreation;
	}
	function setIdRefUniversiteBranche($iduniv){
		$this->idRefUniversiteBranche = $iduniv;
	}
	
	
}
