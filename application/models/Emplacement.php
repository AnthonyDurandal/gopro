<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emplacement extends CI_Model{
	
	private $id;
	private $nom;
	private $idUniversite;

	function __constructor__($id,$nom,$idUniversite){
		$this->setId($id);
		$this->setNom($nom);
		$this->setidUniversite($idUniversite);
		
	}
	function getId(){
		return $this->id;	
	}
	function getNom(){
		return $this->nom;
	}
	function getIdUniversite(){
		return $this->idUniversite;
	}
	
	function setId($id){
		$this->id = $id;	
	}
	function setNom($nom){
		$this->nom = $nom;
	}
	
	function setIdUniversite($idUniversite){
		$this->idUniversite = $idUniversite;
	}
	
	function getEmplacementByIdUniversite($idUniversite)
    {
        $requete = "select * from relEmplacementUniversite where idUniversite=".$idUniversite;
        $result = $this->db->query($requete);
        return $result->row_array();
    }
	function getEmplacementById($id)
    {
        $requete = "select * from emplacement where id=".$id;
        $result = $this->db->query($requete);
		// echo "requete emplacementID".$requete."<br/>";
        return $result->row_array();
    }
	
}
