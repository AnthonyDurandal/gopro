<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Evenement extends DBTable{
	
	private $id;
	private $idTypeEvenement;
	private $idUniversite;
    private $dateDebut;
    private $dateFin;
    private $description;

	function __constructor__($idTypeEvenement,$idUniversite,$dateDebut,$dateFin,$description){
		// $this->setId($id);
		$this->setIdTypeEvenement($idTypeEvenement);
		$this->setIdUniversite($idUniversite);
        $this->setidUniversite($dateDebut);
        $this->setidUniversite($dateFin);
        $this->setDescription($description);
    }
	
	function getId(){
		return $this->id;	
	}
	function getIdTypeEvenement(){
		return $this->idTypeEvenement;
	}
	function getIdUniversite(){
		return $this->idUniversite;
	}
    function getDateDebut() {
        return $this->dateDebut;
    }
    function getDateFin() {
        return $this->dateFin;
    }
    function getDescription() {
        return $this->description;
    }
	
	function setId($id){
		$this->id = $id;	
	}
	function setIdTypeEvenement($idTypeEvenement){
		$this->idTypeEvenement = $idTypeEvenement;
	}
	function setIdUniversite($idUniversite){
		$this->idUniversite = $idUniversite;
	}
    function setDateDebut($dateDebut) {
        $this->dateDebut = $dateDebut; 
    }
    function setDateFin($dateFin) {
        $this->dateFin = $dateFin; 
    }
    function setDescription($description) {
        $this->description = $description; 
    }
}

?>