<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class TypeEvenement extends DBTable{
	
    private $id;
    private $intitule;

	function __constructor__($id,$intitule){
		$this->setId($id);
        $this->setIntitule($intitule);
    }
    
	function getId(){
		return $this->id;	
    }
    function getIntitule(){
		return $this->intitule;	
    }

	function setId($id){
		$this->id = $id;	
	}
	function setIntitule($intitule){
		$this->intitule = $intitule;	
	}
	
}
?>