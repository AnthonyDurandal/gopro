INSERT INTO universite(id,nom,pathLogo,slogan,description,anneeCreation,login,mdp) VALUES
    (1,'Universite d Antananarivo','pathLogo','slogan','description',1860,'login','mdp'),
    (2,'IT University','pathLogo','slogan','description',1940,'login','mdp'),
    (3,'ESCA','pathLogo','slogan','description',1960,'login','mdp');

INSERT INTO filiere(id,intitule,descriptionFiliere) VALUES
    (1,'Droit','C’est l’ensemble des règles imposées aux membres d’une société pour que leurs rapports sociaux échappent à l’arbitraire et à la violence des individus et soient conformes à l’éthique dominante.'),
    (2,'Informatique','L informatique est un domaine d activité scientifique, technique, et industriel concernant le traitement automatique de l information numérique par l exécution de programmes informatiques par des machines : des systèmes embarqués, des ordinateurs, des robots, des automates, etc.'),
    (3,'Aeronotique','C’est l’ensemble des règles imposées aux membres d’une société pour que leurs rapports sociaux échappent à l’arbitraire et à la violence des individus et soient conformes à l’éthique dominante.'),
    (4,'Agronomie','L’agronomie est l ensemble des sciences exactes, naturelles, économiques et sociales, et des techniques auxquelles il est fait appel dans la pratique et la compréhension de l agriculture. Les sciences vétérinaires sont parfois exclues de cette définition.'),
    (5,'Comptabilite','Aujourd’hui, la comptabilité peut être définie comme « un système d’information et plus précisément un système formel d’identification, de mesure, de classement, d’enregistrement des transactions des organisations destiné à fournir après traitement approprié des informations susceptibles de satisfaire les besoins présumés de multiples utilisateurs');
    (6,'Bâtiment et travaux publics','Le BTP regroupe toutes les activités de conception et de construction des bâtiments publics et privés, industriels ou non, et des infrastructures telles que les routes ou les canalisations.'),
    (7,'Multimédia','Ensemble des techniques et des produits qui permettent l’utilisation simultanée et interactive de plusieurs modes de représentation de l’information (textes, sons, images fixes ou animées,…).'),
    (8,'Economie','L’économie (ou économie politique, science économique) est une discipline qui étudie l’économie en tant qu’activité humaine, qui consiste en la production, la distribution, l’échange et la consommation de biens et de services.');

INSERT INTO branche(id,idFiliere,intitule,descriptionBranche) VALUES
    -- DROIT
    (1,1,'Droit public','Le droit public renferme toutes les règles qui concernent l’organisation et le fonctionnement de l’Etat, de l’administration et des collectivités territoriales. Le droit public est également constitué de rapports qui existent entre les pouvoirs publics et les particuliers.'),
    (2,1,'Droit privé','Le droit privé comprend l’ensemble des règles de droit qui concernent les rapports entre les personnes. Il encadre leurs actes et s’applique aux personnes physiques (les particuliers) mais aussi aux personnes morales (les associations et les sociétés).'),
    (3,1,'Droit international public','Le droit international public régit les relations entre les Etats et les organisations internationales. Il se base sur les conversations et les traités internationaux. Ces textes peuvent avoir un caractère bilatéral (qui engage deux Etats) ou multilatéral (qui engage plus de deux Etats). Il intervient dans les conflits et guerres, le commerce ainsi que dans certaines parties du globe comme les mers, l’espace ou l’Antarctique.'),
    (4,1,'Droit international privé','Le droit international privé a pour objet de régler les rapports entre des personnes privées de différentes nationalités ou vivant dans des Etats différents. Il tente d’apporter des réponses aux litiges de juridictions, de compétences et aux conflits des lois.'),
    (5,1,'Droit interne','Le droit interne peut aussi être appelé droit national car il concerne les rapports sociaux à l’intérieur d’un Etat.'),

    -- INFORMATIQUE
    (6,2,'Réseau et systeme ','Le droit public renferme toutes les règles qui concernent l’organisation et le fonctionnement de l’Etat, de l’administration et des collectivités territoriales. Le droit public est également constitué de rapports qui existent entre les pouvoirs publics et les particuliers.'),
    (7,2,'Développement web ','La programmation web est la programmation informatique qui permet d éditer des sites web. Elle permet la création d applications, destinées à être déployées sur Internet ou en Intranet.'),
    (8,2,'Développement d’application ','Le développement de logiciel consiste à étudier, concevoir, construire, transformer, mettre au point, maintenir et améliorer des logiciels.Ce travail est effectué par les employés d éditeurs de logiciels, de sociétés de services et d ingénierie informatique (SSII), des travailleurs indépendants (freelance) et des membres de la communauté du logiciel libre.'),
    (9,2,'Cybersécurité','La cybersécurité consiste à protéger les ordinateurs, les serveurs, les appareils mobiles, les systèmes électroniques, les réseaux et les données contre les attaques malveillantes.'),
    (10,2,'Base de donnée ','Une base de données permet de stocker et de retrouver des données structurées, semi-structurées ou des données brutes ou de l information, souvent en rapport avec un thème ou une activité ; celles-ci peuvent être de natures différentes et plus ou moins reliées entre elles.'),

    -- AVIATION
    (11,3,'Pilote','Métier qui consiste à assurer le transport de voyageurs et/ou de marchandise pour des compagnies aériennes. Le pilote d’avion est en charge du pilotage de l’avion. Dans certains cas, il peut travailler pour le compte de missions humanitaires, contre les incendies, etc.'),
    (12,3,'Constructeurs','La construction aéronautique regroupe les métiers de la conception, de la fabrication et de la commercialisation des aéronefs, tels les avions, les hydravions ou les hélicoptères. Elle est une des activités du secteur aéronautique et spatial.'),
    (13,3,'Aéroportuaire','Un aéroport est l’ensemble des bâtiments et des installations qui servent au traitement des passagers ou du fret aérien situés sur un aérodrome. Le bâtiment principal est, généralement, l’aérogare par où transitent les passagers (ou le fret) entre les moyens de transport au sol et les avions.'),
    (14,3,'Hôtesses et Stewards (PCN)','Hôtesse de l’air ou Steward (PCN) accueillent les passagers à bord de l’avion avant de s’assurer de la sécurité et du bon déroulement du vol. En cabine, ils servent les voyageurs, ils informent et veillent à leur confort et à leur sécurité.'),
    (15,3,'Contrôle et circulation aérienne','Un contrôleur de la circulation aérienne ou contrôleur aérien (parfois surnommé aiguilleur du ciel) est une personne chargée d’assurer un service de contrôle de la circulation aérienne. Il exerce son métier dans la vigie d’une tour de contrôle (pour le contrôle d’aérodrome), ou dans une salle de contrôle d’approche (pour les arrivées et les départs) ou dans un centre de contrôle régional (contrôle en route).'),
    
    -- AGRONOMIE
    (16,4,'Phytopathologiste','Personne qui, à titre de spécialiste de la biologie végétale, étudie les maladies des plantes.'),
    (17,4,'Agriculture biologique','L agrobiologie est la science de la nutrition et de la croissance des plantes en relation avec les conditions du sol, en particulier pour déterminer les moyens d augmenter les rendements des cultures.');
    (18,4,'Agroalimentaire',''),
    
    -- COMPTABILITE
    (19,5,'Comptabilité financière','La comptabilité financière englobe la préparation des états financiers. On peut penser a l’état de la situation financière, à l’état des résultat, à l’état des capitaux propres et à l’état des flux de trésorerie. La comptabilité financière d’une entreprise est donc intéressant pour les utilisateurs d’information historique externes, comme les créanciers et les actionnaires d’une société.'),
    (20,5,'Comptabilité de gestion','La comptabilité de gestion, aussi appelée comptabilité de management, est nettement plus détaillé que la comptabilité financière au niveau de la comptabilité interne de la compagnie. C’est un type de comptabilité qui servira à la prise de décision au sein de l’entreprise, donc très utile pour les gestionnaires et les propriétaires d’entreprises. La comptabilité de gestion englobe la fabrication des produits en entreprise et leurs couts ainsi que la prise de décision relative aux opérations interne de l’entreprise. Des thèmes comme les couts fixes et variable, le calcul des marges sur cout variable et les marges de profits, les prix de cession interne (PCI) reviennent souvent en comptabilité de gestion. Contrairement à la comptabilité financière, l’information dégagée par la comptabilité de gestion n’est quasiment jamais communiqué aux utilisateurs externes.'),
    (21,5,'Gestion financière','La gestion financière est la troisième grande branche de la comptabilité. Elle couvre l’application des principes de finances en comptabilité. L’actualisation, le cout du capital et sa structure, le risque et le financement sont des thèmes qui sont touchés par la gestion financière et qui peuvent être très utiles en comptabilité financière.');

    -- BTB
    (22,6,'Bâtiment et électricité','Qu’il travaille dans une entreprise de Bâtiment et Travaux Publics, chez un artisan ou à son compte, l’électricien est chargé d’installer des équipements électriques (éclairages, alarmes, volets roulants…) et réseaux de télécommunications en respectant rigoureusement les normes de sécurité et la réglementation.'),
    (23,6,'Chantiers','Les chantiers sont définis comme « tous lieux où sont exécutés des travaux de bâtiment ou de génie civil concourant à la réalisation d’un même objectif et sur lesquels existe un risque de coactivité ». Ils accueillent des travaux tels que la construction et la démolition.'),
    (24,6,'Plomberie et maçonnerie','La plomberie est une spécialité de l’ingénieur en mécanique appliquée au bâtiment et du plombier spécialisé, regroupant l’ensemble des techniques utilisées pour faire circuler des fluides (liquide ou gaz) à l’aide de tuyaux, tubes, vannes, robinets, soupapes, pompes aux différents points d’usage d’une installation. La maçonnerie est l’art de bâtir une construction par l’assemblage de matériaux élémentaires, liés ou non par un mortier. C’est l’art du maçon par définition mais aussi le résultat de son travail.'),
    
    -- MULTIMEDIA
    (25,7,'Graphisme et design','C’est l’art et la pratique de la planification et de la projection d’idées et d’expériences avec du contenu texte et image.'),
    (26,7,'Audiovisuel','L’audiovisuel désigne à la fois les matériels, techniques et méthodes d’information, de communication ou d’enseignement associant le son et l’image. '),
    (27,7,'Production assistée par ordinateur (PAO)','Le PAO consiste à préparer des documents destinés à l’impression à l’aide d’un ordinateur en lieu et place des procédés historiques de la typographie et de la photocomposition.'),

    -- ECONOMIE
    (28,8,'Microéconomie','La microéconomie (ou micro-économie) est la branche de l’économie qui modélise le comportement des agents économiques (consommateurs, ménages, entreprises, etc.) et leurs interactions notamment sur les marchés.'),
    (29,8,'Macroéconomie','À l’inverse de la microéconomie, la macroéconomie modélise les relations existantes entre les grands agrégats économiques, le revenu national, l’investissement, la consommation, le taux de chômage, l’inflation, etc.'),
    (30,8,'Économie des organisations','L’économie des organisations est une branche de l’économie qui étudie l’ensemble des arrangements institutionnels permettant la mise en œuvre de la production et l’échange de biens et de services.'),
    (31,8,'Économie publique','L’économie publique est une branche de l’économie qui étudie les politiques que devrait mener un État dans le but de développement économique et de bien-être de sa population, et qui étudie également les problématiques d’inégalités internes et de redistribution des richesses.');

INSERT INTO relUniversiteBranche(idUniversite,idBranche) VALUES
    (1,1),
    (1,3),
    (1,5),
    (1,16),
    (1,17),
    (2,11),
    (2,13),
    (2,15),
    (3,6),
    (3,7),
    (3,8),
    (3,10);